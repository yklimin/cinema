﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema
{
    public class Films
    {
        public DateTime date_session { get; set; }
        public string film_name { get; set; }
        public string start_time_film { get; set; }
        public string duration { get; set; }
        public int price { get; set; }
        private List<Sessions> session { get; set; }

        public Films()
        {
            session = new List<Sessions>();
        }

        public int CountSeats 
        {
            get { return session.Count; }
        }

        //Добавление занятых мест
        public bool AddNewSession(Sessions new_sesions)
        {
            foreach(Sessions session in session)
            {
                if (session == new_sesions)
                    return false;
            }
            session.Add(new_sesions);
            return true;
        }

        public IReadOnlyCollection<Sessions> GetSessions()
        {
            return session.AsReadOnly();
        }
    }
}

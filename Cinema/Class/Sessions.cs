﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema
{
    public class Sessions
    {
        public int row { get; set; }
        public int place { get; set; }

        public static bool operator !=(Sessions left, Sessions right)
        {
            return !(left == right);
        }

        public static bool operator ==(Sessions left, Sessions right)
        {
            if (left.row == right.row &&
                left.place == right.place)
                return true;
            else return false;
        }

    }
}

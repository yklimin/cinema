﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema
{
    class MainContr
    {
        //Список Кинотеатров
        private List<CinemaTheatre> cinemas;
        //Делегат событий для обновления
        public delegate void workDone();
        public event workDone update;

        public int CountCinema;
        public MainContr()
        {
            ContrDB.Load_from_file();
            cinemas = new List<CinemaTheatre>(ContrDB.GetCinemas());
            CountCinema = cinemas.Count;
        }
        
        //Метод сохранения БД
        public void Save()
        {
            ContrDB.Save_from_file();
        }


        //Метод добавление кинотеатра
        public void AddCinemaTheatre()
        {
            //cinemas.
            AddCinemaForm addCinema = new AddCinemaForm();
            addCinema.FormClosed += Signal;
            addCinema.Show();
        }

        //Метод удаления кинотеатра
        public void DeleteCinemaTheatre()
        {
            DeleteCinemaForm deleteCinema = new DeleteCinemaForm();
            deleteCinema.FormClosed += Signal;
            deleteCinema.Show();
        }

        //Метод добавления зала
        public void AddHall(string name)
        {
            AddHallForm HallToCinema = new AddHallForm(name);
            HallToCinema.FormClosed += Signal;
            HallToCinema.Show();
        }

        //Метод получения списка Кинотеатров
        public IReadOnlyList<CinemaTheatre> GetCinema()
        {
            return cinemas.AsReadOnly();
        }

        //Обработчик промежуточных форм
        private void Signal(object sender,EventArgs e)
        {
            cinemas = new List<CinemaTheatre>(ContrDB.GetCinemas());
            update();
        }

        //Метод обработки открытие зала
        public void HallInfo(string cinema_theatre_name,string name_hall)
        {
            foreach(CinemaTheatre cinemaOffice in cinemas)
            {
                if(cinemaOffice.cinema_name == cinema_theatre_name)
                {
                    foreach(Halls hal in cinemaOffice.GetHall())
                    {
                        if(hal.hall_number == name_hall)
                        {
                            InfoAboutHall flm_in_Hall = new InfoAboutHall(cinemaOffice, hal,cinemaOffice.start_time_work,cinemaOffice.stop_time_work);
                            flm_in_Hall.FormClosed += Signal;
                            flm_in_Hall.Show();
                        }
                    }
                }
            }
        }
    }
}

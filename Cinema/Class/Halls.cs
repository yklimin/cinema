﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema
{
    public class Halls
    {
        public string hall_number { get; set; }
        public int count_row { get; set; }
        public int count_places { get; set; }
        public int count_seats { get; set; }

        private List<Films> films;

        public Halls()
        {
            films = new List<Films>();    
        }

        //Метод добавления фильма
        public bool AddFilm(Films new_film)
        {
            foreach(Films flm in films)
            {
                if ((flm.date_session == new_film.date_session) && (flm.start_time_film == new_film.start_time_film))
                    return false;
            }
            films.Add(new_film);
            return true;
        }

        //Метод удаления фильма
        public bool DeleteFilm(Films del_film)
        {
            foreach(Films film in films)
            {
                if(film.date_session == del_film.date_session &&
                   film.film_name == del_film.film_name &&
                   film.start_time_film == del_film.start_time_film &&
                   film.duration == del_film.duration &&
                   film.price == del_film.price)
                {
                    return films.Remove(film);
                }
            }            
            return true;
        }

        //Метод прочтения списка фильмов в зале
        public IReadOnlyCollection<Films> GetFilm()
        {
            return films.AsReadOnly();
        }
    }
}

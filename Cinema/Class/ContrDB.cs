﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading.Tasks;

namespace Cinema
{
    static public class ContrDB
    {
        static private List<CinemaTheatre> cinemas = new List<CinemaTheatre>();

        //Чтение файла
        public static void Load_from_file()
        {
            //Открытие БД
            XmlDocument doc = new XmlDocument();
            doc.Load("DataBase.xml");
            //Выделяем память под список
            cinemas = new List<CinemaTheatre>();
            //Проходим по списку офисов в БД
            foreach(XmlNode cinemaNode in doc.ChildNodes[0].ChildNodes)
            {
                //Выделяем память под кинотеатры
                CinemaTheatre cinemaOffice = new CinemaTheatre();
                //Заполняем данные
                cinemaOffice.cinema_name = cinemaNode.ChildNodes[0].InnerText;
                cinemaOffice.phone = cinemaNode.ChildNodes[1].InnerText;
                cinemaOffice.website = cinemaNode.ChildNodes[2].InnerText;
                cinemaOffice.address = cinemaNode.ChildNodes[3].InnerText;
                cinemaOffice.start_time_work = cinemaNode.ChildNodes[4].InnerText;
                cinemaOffice.stop_time_work = cinemaNode.ChildNodes[5].InnerText;
                //Проходим по привязанному списку залов
                foreach(XmlNode hallNode in cinemaNode.ChildNodes[6].ChildNodes)
                {
                    //Выделяем память под залы
                    Halls hall = new Halls();
                    //Заполняем данные
                    hall.hall_number = hallNode.ChildNodes[0].InnerText;
                    hall.count_row = Int32.Parse(hallNode.ChildNodes[1].InnerText);
                    hall.count_places = Int32.Parse(hallNode.ChildNodes[2].InnerText);
                    hall.count_seats = Int32.Parse(hallNode.ChildNodes[3].InnerText);
                    foreach(XmlNode filmNode in hallNode.ChildNodes[4].ChildNodes)
                    {
                        //Выделяем память под фильмы
                        Films film = new Films();
                        //Заполняем данные
                        film.date_session = DateTime.Parse(filmNode.ChildNodes[0].InnerText);
                        film.film_name = filmNode.ChildNodes[1].InnerText;
                        film.start_time_film = filmNode.ChildNodes[2].InnerText;
                        film.duration = filmNode.ChildNodes[3].InnerText;
                        film.price = Int32.Parse(filmNode.ChildNodes[4].InnerText);
                        foreach(XmlNode sessionNode in filmNode.ChildNodes[5].ChildNodes)
                        {
                            //Выделяем память под фильмы
                            Sessions ses = new Sessions();
                            //Заполняем данные
                            ses.row = Int32.Parse(sessionNode.ChildNodes[0].InnerText);
                            ses.place = Int32.Parse(sessionNode.ChildNodes[1].InnerText);
                            //Привязываем выбранные места к фильму
                            film.AddNewSession(ses);
                        }
                        //Привязываем фильмы к залу
                        hall.AddFilm(film);
                    }
                    //Привязываем залы к кинотеатру
                    cinemaOffice.AddHall(hall);
                }
                //Добавляем кинотеатр в список
                cinemas.Add(cinemaOffice);
            }
        }


        //Сохранение в файл
        public static void Save_from_file()
        {
            //Открываем файл для записи
            XmlDocument doc = new XmlDocument();
            doc.Load("DataBase.xml");
            //Очищаем существующую БД для перезаписи
            doc.RemoveAll();
            //Начинаем перезапись
            XmlNode NetNode = doc.CreateElement("Base");
            foreach(CinemaTheatre cinemaOffice in cinemas)
            {
                XmlNode CinemaOfficeNode = doc.CreateElement("cinemas");

                XmlNode CinemaNode = doc.CreateElement("cinema_name");
                CinemaNode.InnerText = cinemaOffice.cinema_name;
                CinemaOfficeNode.AppendChild(CinemaNode);

                XmlNode PhoneNode = doc.CreateElement("phone");
                PhoneNode.InnerText = cinemaOffice.phone;
                CinemaOfficeNode.AppendChild(PhoneNode);

                XmlNode WebSiteNode = doc.CreateElement("website");
                WebSiteNode.InnerText = cinemaOffice.website;
                CinemaOfficeNode.AppendChild(WebSiteNode);

                XmlNode AdressNode = doc.CreateElement("adress");
                AdressNode.InnerText = cinemaOffice.address;
                CinemaOfficeNode.AppendChild(AdressNode);

                XmlNode StartTimeNode = doc.CreateElement("start_time_work");
                StartTimeNode.InnerText = cinemaOffice.start_time_work;
                CinemaOfficeNode.AppendChild(StartTimeNode);

                XmlNode StopTimeNode = doc.CreateElement("stop_time_work");
                StopTimeNode.InnerText = cinemaOffice.stop_time_work;
                CinemaOfficeNode.AppendChild(StopTimeNode);

                XmlNode HallsNode = doc.CreateElement("halls");
                foreach(Halls hall in cinemaOffice.GetHall())
                {
                    XmlNode HallNode = doc.CreateElement("hall");

                    XmlNode HallNumberNode = doc.CreateElement("hall_number");
                    HallNumberNode.InnerText = hall.hall_number;
                    HallNode.AppendChild(HallNumberNode);

                    XmlNode CountRowNode = doc.CreateElement("count_row");
                    CountRowNode.InnerText = hall.count_row.ToString();
                    HallNode.AppendChild(CountRowNode);

                    XmlNode CountPlaceNode = doc.CreateElement("count_place");
                    CountPlaceNode.InnerText = hall.count_places.ToString();
                    HallNode.AppendChild(CountPlaceNode);

                    XmlNode CountSeatsNode = doc.CreateElement("count_seats");
                    CountSeatsNode.InnerText = hall.count_seats.ToString();
                    HallNode.AppendChild(CountSeatsNode);

                    XmlNode FilmsNode = doc.CreateElement("films");
                    foreach(Films film in hall.GetFilm())
                    {
                        XmlNode FilmNode = doc.CreateElement("_film");

                        XmlNode DateSessionNode = doc.CreateElement("date_session");
                        DateSessionNode.InnerText = film.date_session.ToShortDateString().ToString();
                        FilmNode.AppendChild(DateSessionNode);

                        XmlNode FilmNameNode = doc.CreateElement("film_name");
                        FilmNameNode.InnerText = film.film_name;
                        FilmNode.AppendChild(FilmNameNode);

                        XmlNode StartTimeFilmNode = doc.CreateElement("start_time_film");
                        StartTimeFilmNode.InnerText = film.start_time_film;
                        FilmNode.AppendChild(StartTimeFilmNode);

                        XmlNode DurationNode = doc.CreateElement("duration");
                        DurationNode.InnerText = film.duration;
                        FilmNode.AppendChild(DurationNode);

                        XmlNode PriceNode = doc.CreateElement("price");
                        PriceNode.InnerText = film.price.ToString();
                        FilmNode.AppendChild(PriceNode);

                        XmlNode SessionsNode = doc.CreateElement("sessions");
                        foreach(Sessions ses in film.GetSessions())
                        {
                            XmlNode SessionNode = doc.CreateElement("session");

                            XmlNode RowNode = doc.CreateElement("row");
                            RowNode.InnerText = ses.row.ToString();
                            SessionNode.AppendChild(RowNode);

                            XmlNode PlaceNode = doc.CreateElement("row");
                            PlaceNode.InnerText = ses.place.ToString();
                            SessionNode.AppendChild(PlaceNode);

                            SessionsNode.AppendChild(SessionNode);
                        }
                        FilmNode.AppendChild(SessionsNode);
                        FilmsNode.AppendChild(FilmNode);
                    }
                    HallNode.AppendChild(FilmsNode);
                    HallsNode.AppendChild(HallNode);
                }
                CinemaOfficeNode.AppendChild(HallsNode);
                NetNode.AppendChild(CinemaOfficeNode);
            }
            doc.AppendChild(NetNode);
            //Сохраняем файл
            doc.Save("DataBase.xml");
        }

        //Метод добавление Кинотеатра
        public static bool AddCinema(CinemaTheatre _cinema)
        {
            foreach( CinemaTheatre cinemaOffice in cinemas)
            {
                if (cinemaOffice.cinema_name == _cinema.cinema_name)
                    return false;
            }
            cinemas.Add(_cinema);
            return true;
        }

        //Метод удаление Кинотеатра
        public static void DeleteCinema(string _cinema_name)
        {
            foreach(CinemaTheatre cinemaOffice in cinemas)
            {
                if(cinemaOffice.cinema_name == _cinema_name)
                {
                    cinemas.Remove(cinemaOffice);
                    break;
                }
            }
        }

        //Метод добавления зала
        public static bool AddHallInCinema(string _hall_name,Halls _new_hall)
        {
            foreach(CinemaTheatre cinemaOffice in cinemas)
            {
                if (cinemaOffice.cinema_name == _hall_name)
                {
                    return cinemaOffice.AddHall(_new_hall);
                }
            }
            return false;
        }

        //Метод удаления Зала
        public static void DeleteHall(string _cinema_name, string hall_name)
        {
            foreach(CinemaTheatre cinemaOffice in cinemas)
            {
                if(cinemaOffice.cinema_name == _cinema_name)
                {
                    cinemaOffice.DeleteHall(hall_name);
                    break;                   
                }
            }
        }


        //Метод добавления Фильма в зал
        public static void AddFilm(string _cinema_name, string _hall_name, Films _film)
        {
            foreach(CinemaTheatre cinemaOffice in cinemas)
            {
                if(cinemaOffice.cinema_name == _cinema_name)
                {
                    foreach(Halls hal in cinemaOffice.GetHall())
                    {
                        if(hal.hall_number == _hall_name)
                        {
                            hal.AddFilm(_film);
                        }
                    }
                }
            }
        }

        //Метод удаления фирмы из зала
        public static void DeleteFilm(string _cinema_name, string _hall_name, Films _film)
        {
            foreach(CinemaTheatre cinemaOffice in cinemas)
            {
                if(cinemaOffice.cinema_name == _cinema_name)
                {
                    foreach(Halls hal in cinemaOffice.GetHall())
                    {
                        if(hal.hall_number == _hall_name)
                        {
                            hal.DeleteFilm(_film);
                        }
                    }
                }
            }
        }

        //Метод добавление мест
        public static void AddSession(string _cinema_name, string _hall_name, Films _film, Sessions _session)
        {
            foreach (CinemaTheatre cinemaOffice in cinemas)
            {
                if (cinemaOffice.cinema_name == _cinema_name)
                {
                    foreach (Halls hal in cinemaOffice.GetHall())
                    {
                        if (hal.hall_number == _hall_name)
                        {
                            foreach(Films film in hal.GetFilm())
                            {
                                if(film.film_name == _film.film_name &&
                                    film.date_session == _film.date_session &&
                                    film.duration == _film.duration &&
                                    film.start_time_film == _film.start_time_film &&
                                    film.price == _film.price)
                                {
                                    film.AddNewSession(_session);
                                }
                            }
                        }
                    }
                }
            }


        }



        //Метод удаление кинотеатра
        public static IReadOnlyCollection<CinemaTheatre> GetCinemas()
        {
            return cinemas.AsReadOnly();
        }

    }
}

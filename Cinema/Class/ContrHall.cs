﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema
{
    class ContrHall
    {
        //Делегат для обновления
        public delegate void workDone();
        public event workDone update;
        //Открытый зал
        private Halls hal;
        //Зал кинотеатра
        private string cinema_name;

        //Конструктор
        public ContrHall(Halls _hal,string _cinema_name)
        {
            hal = new Halls();
            cinema_name = _cinema_name;
            hal = _hal;
        }

        public void DelHall()
        {
            ContrDB.DeleteHall(cinema_name, hal.hall_number);
        }


        //Добавление фильма в зале
        public bool AddFilmToHall(string date, string films_name, string st_time, string duration, string price,string time_open,string time_close)
        {
            
            DateTime dateCheck = ParseTime(date, st_time);
            bool result = CheckNewData(dateCheck, time_open, time_close, st_time, duration);
            if (!result)
            {
                return false;
            }
            //Добавление фильма
            Films film = new Films();
            DateTime dateTime = ParseTime(date, "00:00");
            film.date_session = dateTime;
            film.film_name = films_name;
            film.start_time_film = st_time;
            film.duration = duration;
            film.price = Int32.Parse(price);
            //Добавляем в БД
            ContrDB.AddFilm(cinema_name, hal.hall_number, film);

            return true;
        }

        //Удаление фильма в зале
        public bool DeleteFilmInHall(string date, string films_name, string st_time, string dur, string pr)
        {
            //Получаем дату и время когда будет идти фильм
            DateTime dateCheck = ParseTime(date, st_time);

            //Передаём для проверки
            bool result = CheckDelData(dateCheck, st_time, dur);
            if(!result)
            {
                return false;
            }
            dateCheck = ParseTime(date, "00:00");
            Films film = new Films();
            film.date_session = dateCheck;
            film.film_name = films_name;
            film.start_time_film = st_time;
            film.duration = dur;
            film.price = Int32.Parse(pr);
            //Добавляем в БД
            ContrDB.DeleteFilm(cinema_name, hal.hall_number, film);

            return true;
        }

        //Обработчик закрытия промежуточных форм
        public void Update(Object obj, EventArgs e)
        {

            update();
        }

        //Проверка при добавлении элементов
        public bool CheckNewData(DateTime date, string open, string close,string time, string duration)
        {
            //Проверка, на добавление сеанса фильм, раньше текущей даты и времени, когда кинотеатр закрыт
            DateTime dateNow = DateTime.Now;
            DateTime dateCheckOpen = ParseTime(date.ToShortDateString().ToString(), open);
            DateTime dateCheckClose = ParseTime(date.ToShortDateString().ToString(), close);
            if (dateNow > date || dateCheckOpen > date || dateCheckClose < date) return false;

            //Дополнительная проверка на завершении показа фильма, т.к. кинотеатр не может работать дольше, чеме график его работы
            DateTime dateCheckDuration = ParseTime(date.ToShortDateString().ToString(), duration);
            DateTime dateCheckEndWorkCinema = ParseTime(dateNow.ToShortDateString().ToString(), time);
            dateCheckEndWorkCinema = dateCheckEndWorkCinema.AddHours(dateCheckDuration.Hour);
            dateCheckEndWorkCinema = dateCheckEndWorkCinema.AddMinutes(dateCheckDuration.Minute);
            if (dateCheckClose < dateCheckEndWorkCinema) return false;

            foreach (Films film in hal.GetFilm())
            {
                //Проверка дня
                if (film.date_session.ToShortDateString() == date.ToShortDateString())
                {
                    //Создаем переменные для проверки вхождения в интервалы сеанса
                    DateTime dateCheckInExistFilm = ParseTime(dateNow.ToShortDateString().ToString(),film.start_time_film);
                    DateTime dateCheckInExistDuration = ParseTime(dateNow.ToShortDateString().ToString(), film.duration);
                    //Добавляем 15 минут (время на уборку кинозала)
                    dateCheckInExistDuration = dateCheckInExistDuration.AddMinutes(15);

                    //Финальные преобразования
                    string dateCheck = date.Subtract(dateCheckInExistFilm).ToString();
                    DateTime finaldataCheck = ParseTime(dateNow.ToShortDateString().ToString(), dateCheck);

                    if(dateCheckInExistDuration >= finaldataCheck)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        //Проверка при удалении
        //Проверку нельзя реализвать, из-за некорректного вывода
        public bool CheckDelData(DateTime date, string st_time, string duration)
        {
            //Проверяем на удаление фильма, если он на день раньше или на день больше
            //Сначала берём текущее время
            DateTime dateNow = DateTime.Now;

            DateTime dateCheckNow = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day);
            DateTime dateCheckFilm = new DateTime(date.Year, date.Month, date.Day);
            //Вычитание двух одинаковых дней даст нулевое время, т.е. мы будем удалять либо наступивший день, либо ещё не наступивший
            if (dateCheckFilm.Subtract(dateCheckNow).ToString() == "00:00:00")
            {
                //Добавляем к фильму его длительность, а также 15 минут на уборку зала
                DateTime dateCheckInExistDuration = ParseTime(date.ToShortDateString().ToString(), duration);
                date = date.AddHours(dateCheckInExistDuration.Hour);
                date = date.AddMinutes(dateCheckInExistDuration.Minute + 15);

                
                string check = dateNow.Subtract(date).ToString();
                //Если мы найдем "-", значит фильм ещё идёт, т.к. время должно быть положительно чтобы фильм кончился
                for (int i = 0; i < check.Length; i++)
                {
                    if (check[i] == '-')
                        return false;
                }

            }
            else return true;

            return true;
        }
        
        //Открытие окна выбора места
        

        //Метод преобразования времени
        public DateTime ParseTime(string date, string times)
        {
            List<string> vs = new List<string>();
            string ch = "";
            for (int i = 0; i < date.Length; i++)
            {
                if (date[i] == '.')
                {
                    if (ch == "")
                        vs.Add("00");
                    vs.Add(ch);
                    ch = "";
                    i++;
                }
                ch += date[i];
            }
            if (ch == "")
                vs.Add("0000");
            else
                vs.Add(ch);
            ch = "";


            int day = 0;
            for(int i=0; i<times.Length;i++)
            {
                if(times[i]=='.')
                {
                    day = Int32.Parse(ch);
                    ch = "";
                    i++;
                }
                ch += times[i];
            }
            times = ch;
            ch = "";

            for (int i = 0; i < times.Length; i++)
            {
                if (times[i] == ':')
                {
                    if (ch == "")
                        vs.Add("00");
                    if(Int32.Parse(ch)<0)
                    {
                        vs.Add((Math.Abs(Int32.Parse(ch))).ToString());
                    }
                    else
                        vs.Add(ch);
                    ch = "";
                    i++;
                }
                ch += times[i];
            }
            if (ch == "")
                vs.Add("00");
            else
                vs.Add(ch);
            vs.Add("00");

            if (day != 0)
            {
                DateTime timeNow = new DateTime(Int32.Parse(vs[2]), Int32.Parse(vs[1]), Int32.Parse(vs[0]), 00, 00, 00);
                timeNow = timeNow.AddDays(day);
                return timeNow;
            }

            DateTime time = new DateTime(Int32.Parse(vs[2]), Int32.Parse(vs[1]), Int32.Parse(vs[0]), Int32.Parse(vs[3]), Int32.Parse(vs[4]), Int32.Parse(vs[5]));
            

            return time;
        }
    }
}

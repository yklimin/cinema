﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema
{
    public class CinemaTheatre
    {
        public string cinema_name { get; set; }
        public string phone { get; set; }
        public string website { get; set; }
        public string address { get; set; }
        public string start_time_work { get; set; }
        public string stop_time_work { get; set; }
        //Список привязанных Залов
        private List<Halls> halls;

        //Количество залов в кинотеатре
        public int CountHall
        {
            get { return halls.Count; }
        }

        public CinemaTheatre()
        {
            halls = new List<Halls>();
        }

        //Добавление списка Залов
        public bool AddHall(Halls new_hall)
        {
            foreach (Halls hal in halls)
            {
                if (hal.hall_number == new_hall.hall_number)
                    return false;
            }
            halls.Add(new_hall);
            return true;
        }

        //Удаление списка Залов
        public bool DeleteHall(string name_hall)
        {
            foreach(Halls hal in halls)
            {
                if (name_hall==hal.hall_number)
                {
                    return halls.Remove(hal);
                }
            }

            return false;
        }

        //Реализовать

        //Метод получения списка для чтения
        public IReadOnlyCollection<Halls> GetHall()
        {
            return halls.AsReadOnly();
        }

    }
}

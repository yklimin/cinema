﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cinema
{
    public partial class SessionsForm : Form
    {
        //Будем бронировать места используя CheckBox
        CheckBox[,] box;
        //Открытый фильм, передаём доступ к нему для выбора мест
        string cinema_name;
        string hall_name;
        Films film;
        //количество рядов и мест в ряду для данного зала
        int row;
        int place;

        //Данные для выведения CheckBox через систему координат
        int[] x_pos = { 60, 110, 160, 210, 260, 310, 360, 410, 460, 510, 560, 610, 660 };
        int y_pos = 50;


        int index = 0;
        bool result = true;

        public SessionsForm(string _cinema_name,string _hall_name,Films _film, int _row, int _place, bool _result)
        {
            InitializeComponent();
            cinema_name = _cinema_name;
            hall_name = _hall_name;
            film = _film;
            row = _row;
            place = _place;
            result = _result;

           

            //Инициализируем CheckBoxs
            box = new CheckBox[row, place];
            AddCheckBoxToScreen();
            if (!result)
                MessageBox.Show("Данный фильм уже прошёл или уже идёт, окно открыто только для просмотра", "Информация", MessageBoxButtons.OK);
            //Нужен для обновления таблицы и закрашивания уже выбранных мест
            Update();

            lblDate.Text = film.date_session.ToShortDateString().ToString();
            lblStartFilm.Text = film.start_time_film;
            lblFilmName.Text = film.film_name;

        }

        //Вывод CheckBox для выбора мест
        void AddCheckBoxToScreen()
        {
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < place; j++)
                {
                    box[i, j] = new CheckBox();
                    box[i, j].Width = 30;
                    box[i, j].Height = 30;
                    box[i, j].Checked = true;
                    box[i, j].Appearance = Appearance.Button;
                    box[i, j].Text = (j + 1).ToString();

                    box[i, j].Left = x_pos[index++];
                    box[i, j].Top = y_pos;
                    box[i, j].Click += new System.EventHandler(this.button2_Click);
                    box[i, j].BackColor = Color.Green;
                    this.Controls.Add(box[i, j]);
                    if (!result)
                        box[i, j].Enabled = false;
                }
                index = 0;
                y_pos += 50;

            }
            if(row == 5)
            {
                lblLevel6Left.Text = "";
                lblLevel7Left.Text = "";
                lblLevel8Left.Text = "";
                lblLevel9Left.Text = "";
                lblLevel6Right.Text = "";
                lblLevel7Right.Text = "";
                lblLevel8Right.Text = "";
                lblLevel9Right.Text = "";
            }
            if(row ==6)
            { 
                lblLevel7Left.Text = "";
                lblLevel8Left.Text = "";
                lblLevel9Left.Text = "";
                lblLevel7Right.Text = "";
                lblLevel8Right.Text = "";
                lblLevel9Right.Text = "";
            }
            if(row == 7)
            {
                lblLevel8Left.Text = "";
                lblLevel9Left.Text = "";
                lblLevel8Right.Text = "";
                lblLevel9Right.Text = "";
            }
            if(row == 8)
            {
                lblLevel8Left.Text = "";
                lblLevel8Right.Text = "";
            }

        }

        //Выбранные места окрашиваются в жёлтый
        private void button2_Click(object sender, EventArgs e)
        {
            if (result)
            {
                for (int i = 0; i < row; i++)
                {
                    for (int j = 0; j < place; j++)
                    {
                        if (box[i, j].Checked != true && box[i,j].Enabled != false)
                        {
                            box[i, j].BackColor = Color.Yellow;
                            Update();
                        }
                        if (box[i, j].Checked == true && box[i, j].Enabled != false)
                        {
                            box[i, j].BackColor = Color.Green;
                        }
                    }
                }
            }
        }

        //Добавление мест в систему фильма
        private void AddNewSession_Click(object sender, EventArgs e)
        {
            if (result)
            {
                for (int i = 0; i < row; i++)
                {
                    for (int j = 0; j < place; j++)
                    {
                        if (box[i, j].Checked != true)
                        {
                            Sessions sesion = new Sessions();
                            sesion.row = i + 1;
                            sesion.place = j + 1;
                            ContrDB.AddSession(cinema_name, hall_name, film, sesion);
                            box[i, j].Enabled = false;
                            box[i, j].BackColor = Color.Red;
                        }
                    }
                }
                Update();
            }
        }

        //Обновление таблицы
        public void Update()
        {
            foreach(Sessions session in film.GetSessions())
            {
                box[session.row - 1, session.place - 1].Enabled = false;
                box[session.row - 1, session.place - 1].BackColor = Color.Red;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
    }
}

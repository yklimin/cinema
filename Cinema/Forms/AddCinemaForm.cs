﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Cinema
{
    public partial class AddCinemaForm : Form
    {
        public AddCinemaForm()
        {
            InitializeComponent();
        }

        //Добавление кинотеатра
        private void AddNewCinemaTheatre_Click(object sender, EventArgs e)
        {
            //Проверка на заполнение полей
            if (txtBoxNewNameCinema.TextLength > 0 &&
               txtBoxNewNumber.TextLength > 0 &&
               txtBoxNewWebSite.TextLength > 0 &&
               txtBoxNewAddress.TextLength>0 &&
               txtBoxNewStartTimeWork.TextLength > 0 &&
               txtBoxNewStopTimeWork.TextLength > 0)
            {
                CinemaTheatre cinema = new CinemaTheatre();
                //Заполняем данные
                cinema.cinema_name = txtBoxNewNameCinema.Text;
                cinema.phone = txtBoxNewNumber.Text;
                cinema.website = txtBoxNewWebSite.Text;
                cinema.address = txtBoxNewAddress.Text;
                cinema.start_time_work = txtBoxNewStartTimeWork.Text;
                cinema.stop_time_work = txtBoxNewStopTimeWork.Text;
                //Добавляем данные
                ContrDB.AddCinema(cinema);
                //Закрываем окно
                Close();
            }
            else
            {
                MessageBox.Show("Не все поля заполнены!", "Поля не заполнены", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Очистка всех полей
        private void ClearButton_Click(object sender, EventArgs e)
        {
            txtBoxNewNameCinema.Text = "";
            txtBoxNewNumber.Text = "";
            txtBoxNewWebSite.Text = "";
            txtBoxNewAddress.Text = "";
            txtBoxNewStartTimeWork.Text = "";
            txtBoxNewStopTimeWork.Text = "";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace Cinema
{
    public partial class DeleteCinemaForm : Form
    {
        public DeleteCinemaForm()
        {
            InitializeComponent();
        }

        private void DeleteCinemaTheatre_Click(object sender, EventArgs e)
        {
            //Если поля не заполнены
            if (txtBoxDeleteNameCinema.TextLength >0)
            {
                ContrDB.DeleteCinema(txtBoxDeleteNameCinema.Text);
                Close();
            }
            else
            {
                MessageBox.Show("Не все поля заполнены!", "Поля не заполнены", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}

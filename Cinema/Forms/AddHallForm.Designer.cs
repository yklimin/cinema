﻿namespace Cinema
{
    partial class AddHallForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ClearButton = new System.Windows.Forms.Button();
            this.txtBoxNewPlace = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBoxNewRow = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBoxNewNameHall = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.AddNewHall = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ClearButton
            // 
            this.ClearButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ClearButton.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ClearButton.Location = new System.Drawing.Point(346, 262);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(142, 38);
            this.ClearButton.TabIndex = 31;
            this.ClearButton.Text = "Очистить";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // txtBoxNewPlace
            // 
            this.txtBoxNewPlace.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtBoxNewPlace.Location = new System.Drawing.Point(220, 92);
            this.txtBoxNewPlace.Name = "txtBoxNewPlace";
            this.txtBoxNewPlace.Size = new System.Drawing.Size(268, 37);
            this.txtBoxNewPlace.TabIndex = 30;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(12, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(195, 35);
            this.label3.TabIndex = 29;
            this.label3.Text = "Количество мест в ряду";
            // 
            // txtBoxNewRow
            // 
            this.txtBoxNewRow.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtBoxNewRow.Location = new System.Drawing.Point(220, 49);
            this.txtBoxNewRow.Name = "txtBoxNewRow";
            this.txtBoxNewRow.Size = new System.Drawing.Size(268, 37);
            this.txtBoxNewRow.TabIndex = 28;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 35);
            this.label2.TabIndex = 27;
            this.label2.Text = "Количество рядов";
            // 
            // txtBoxNewNameHall
            // 
            this.txtBoxNewNameHall.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtBoxNewNameHall.Location = new System.Drawing.Point(220, 6);
            this.txtBoxNewNameHall.Name = "txtBoxNewNameHall";
            this.txtBoxNewNameHall.Size = new System.Drawing.Size(268, 37);
            this.txtBoxNewNameHall.TabIndex = 26;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 35);
            this.label1.TabIndex = 25;
            this.label1.Text = "Название зала";
            // 
            // AddNewHall
            // 
            this.AddNewHall.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.AddNewHall.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddNewHall.Location = new System.Drawing.Point(22, 262);
            this.AddNewHall.Name = "AddNewHall";
            this.AddNewHall.Size = new System.Drawing.Size(142, 38);
            this.AddNewHall.TabIndex = 24;
            this.AddNewHall.Text = "Добавить";
            this.AddNewHall.UseVisualStyleBackColor = true;
            this.AddNewHall.Click += new System.EventHandler(this.AddNewHall_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(476, 35);
            this.label4.TabIndex = 32;
            this.label4.Text = "(При добавлении зала учтите: рядов может быть не менее 5,";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(12, 172);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(370, 35);
            this.label5.TabIndex = 33;
            this.label5.Text = "но не блоее чем 9. А мест в ряду минимум 9, а";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(12, 204);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(118, 35);
            this.label6.TabIndex = 34;
            this.label6.Text = "максимум 13)";
            // 
            // AddHallForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 319);
            this.ControlBox = false;
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.txtBoxNewPlace);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBoxNewRow);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtBoxNewNameHall);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AddNewHall);
            this.MaximumSize = new System.Drawing.Size(530, 366);
            this.MinimumSize = new System.Drawing.Size(530, 366);
            this.Name = "AddHallForm";
            this.Text = "Добавление Зала";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.TextBox txtBoxNewPlace;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBoxNewRow;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBoxNewNameHall;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button AddNewHall;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}
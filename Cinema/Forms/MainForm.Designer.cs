﻿namespace Cinema
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.MenuStripMainForm = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SaveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CloseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.управлениеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.кинотеатрToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AddCinemaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DeleteCinemaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьЗалToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblStopTimeWork = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblStartTimeWork = new System.Windows.Forms.Label();
            this.lblWebsite = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblAdress = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblCinemaName = new System.Windows.Forms.Label();
            this.CinemaTabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.MenuStripMainForm.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.CinemaTabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // MenuStripMainForm
            // 
            this.MenuStripMainForm.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.MenuStripMainForm.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.управлениеToolStripMenuItem});
            this.MenuStripMainForm.Location = new System.Drawing.Point(0, 0);
            this.MenuStripMainForm.Name = "MenuStripMainForm";
            this.MenuStripMainForm.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.MenuStripMainForm.Size = new System.Drawing.Size(981, 28);
            this.MenuStripMainForm.TabIndex = 0;
            this.MenuStripMainForm.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SaveToolStripMenuItem,
            this.CloseToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(59, 26);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // SaveToolStripMenuItem
            // 
            this.SaveToolStripMenuItem.Name = "SaveToolStripMenuItem";
            this.SaveToolStripMenuItem.Size = new System.Drawing.Size(273, 26);
            this.SaveToolStripMenuItem.Text = "Сохранение информации";
            this.SaveToolStripMenuItem.Click += new System.EventHandler(this.SaveToolStripMenuItem_Click);
            // 
            // CloseToolStripMenuItem
            // 
            this.CloseToolStripMenuItem.Name = "CloseToolStripMenuItem";
            this.CloseToolStripMenuItem.Size = new System.Drawing.Size(273, 26);
            this.CloseToolStripMenuItem.Text = "Выход";
            this.CloseToolStripMenuItem.Click += new System.EventHandler(this.CloseToolStripMenuItem_Click);
            // 
            // управлениеToolStripMenuItem
            // 
            this.управлениеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.кинотеатрToolStripMenuItem,
            this.добавитьЗалToolStripMenuItem});
            this.управлениеToolStripMenuItem.Name = "управлениеToolStripMenuItem";
            this.управлениеToolStripMenuItem.Size = new System.Drawing.Size(108, 26);
            this.управлениеToolStripMenuItem.Text = "Управление";
            // 
            // кинотеатрToolStripMenuItem
            // 
            this.кинотеатрToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddCinemaToolStripMenuItem,
            this.DeleteCinemaToolStripMenuItem});
            this.кинотеатрToolStripMenuItem.Name = "кинотеатрToolStripMenuItem";
            this.кинотеатрToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.кинотеатрToolStripMenuItem.Text = "Кинотеатр";
            // 
            // AddCinemaToolStripMenuItem
            // 
            this.AddCinemaToolStripMenuItem.Name = "AddCinemaToolStripMenuItem";
            this.AddCinemaToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.AddCinemaToolStripMenuItem.Text = "Добавить";
            this.AddCinemaToolStripMenuItem.Click += new System.EventHandler(this.AddCinemaToolStripMenuItem_Click);
            // 
            // DeleteCinemaToolStripMenuItem
            // 
            this.DeleteCinemaToolStripMenuItem.Name = "DeleteCinemaToolStripMenuItem";
            this.DeleteCinemaToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.DeleteCinemaToolStripMenuItem.Text = "Удалить";
            this.DeleteCinemaToolStripMenuItem.Click += new System.EventHandler(this.DeleteCinemaToolStripMenuItem_Click);
            // 
            // добавитьЗалToolStripMenuItem
            // 
            this.добавитьЗалToolStripMenuItem.Name = "добавитьЗалToolStripMenuItem";
            this.добавитьЗалToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.добавитьЗалToolStripMenuItem.Text = "Добавить зал";
            this.добавитьЗалToolStripMenuItem.Click += new System.EventHandler(this.NewHallToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.lblStopTimeWork);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lblStartTimeWork);
            this.groupBox1.Controls.Add(this.lblWebsite);
            this.groupBox1.Controls.Add(this.lblPhone);
            this.groupBox1.Controls.Add(this.lblAdress);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.lblCinemaName);
            this.groupBox1.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.MenuText;
            this.groupBox1.Location = new System.Drawing.Point(12, 31);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(957, 304);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Кинотеатр";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Oswald", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label7.Location = new System.Drawing.Point(205, 250);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(115, 41);
            this.label7.TabIndex = 13;
            this.label7.Text = "09.12.2017";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Oswald", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label6.Location = new System.Drawing.Point(15, 250);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(139, 41);
            this.label6.TabIndex = 12;
            this.label6.Text = "Текущая дата";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(544, 18);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(397, 265);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // lblStopTimeWork
            // 
            this.lblStopTimeWork.AutoSize = true;
            this.lblStopTimeWork.Font = new System.Drawing.Font("Oswald", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblStopTimeWork.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblStopTimeWork.Location = new System.Drawing.Point(309, 210);
            this.lblStopTimeWork.Name = "lblStopTimeWork";
            this.lblStopTimeWork.Size = new System.Drawing.Size(71, 41);
            this.lblStopTimeWork.TabIndex = 10;
            this.lblStopTimeWork.Text = "00.00";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Oswald", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label5.Location = new System.Drawing.Point(283, 209);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 41);
            this.label5.TabIndex = 9;
            this.label5.Text = "-";
            // 
            // lblStartTimeWork
            // 
            this.lblStartTimeWork.AutoSize = true;
            this.lblStartTimeWork.Font = new System.Drawing.Font("Oswald", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblStartTimeWork.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblStartTimeWork.Location = new System.Drawing.Point(205, 210);
            this.lblStartTimeWork.Name = "lblStartTimeWork";
            this.lblStartTimeWork.Size = new System.Drawing.Size(71, 41);
            this.lblStartTimeWork.TabIndex = 8;
            this.lblStartTimeWork.Text = "00.00";
            // 
            // lblWebsite
            // 
            this.lblWebsite.AutoSize = true;
            this.lblWebsite.Font = new System.Drawing.Font("Oswald", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblWebsite.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblWebsite.Location = new System.Drawing.Point(205, 170);
            this.lblWebsite.Name = "lblWebsite";
            this.lblWebsite.Size = new System.Drawing.Size(75, 41);
            this.lblWebsite.TabIndex = 7;
            this.lblWebsite.Text = "//сайт";
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Font = new System.Drawing.Font("Oswald", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblPhone.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblPhone.Location = new System.Drawing.Point(205, 130);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(114, 41);
            this.lblPhone.TabIndex = 6;
            this.lblPhone.Text = "//Телефон";
            // 
            // lblAdress
            // 
            this.lblAdress.AutoSize = true;
            this.lblAdress.Font = new System.Drawing.Font("Oswald", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblAdress.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblAdress.Location = new System.Drawing.Point(205, 90);
            this.lblAdress.Name = "lblAdress";
            this.lblAdress.Size = new System.Drawing.Size(101, 41);
            this.lblAdress.TabIndex = 5;
            this.lblAdress.Text = "//Адресс";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Oswald", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label4.Location = new System.Drawing.Point(15, 210);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(146, 41);
            this.label4.TabIndex = 4;
            this.label4.Text = "Время работы";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Oswald", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label3.Location = new System.Drawing.Point(15, 170);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 41);
            this.label3.TabIndex = 3;
            this.label3.Text = "Сайт";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Oswald", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label2.Location = new System.Drawing.Point(15, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 41);
            this.label2.TabIndex = 2;
            this.label2.Text = "Телефон";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Oswald", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label1.Location = new System.Drawing.Point(15, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 41);
            this.label1.TabIndex = 1;
            this.label1.Text = "Адрес";
            // 
            // lblCinemaName
            // 
            this.lblCinemaName.AutoSize = true;
            this.lblCinemaName.Font = new System.Drawing.Font("Oswald", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCinemaName.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblCinemaName.Location = new System.Drawing.Point(5, 27);
            this.lblCinemaName.Name = "lblCinemaName";
            this.lblCinemaName.Size = new System.Drawing.Size(346, 68);
            this.lblCinemaName.TabIndex = 0;
            this.lblCinemaName.Text = "Название кинотеатра";
            this.lblCinemaName.Click += new System.EventHandler(this.lblCinemaName_Click);
            // 
            // CinemaTabControl
            // 
            this.CinemaTabControl.Controls.Add(this.tabPage1);
            this.CinemaTabControl.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CinemaTabControl.Location = new System.Drawing.Point(12, 357);
            this.CinemaTabControl.Name = "CinemaTabControl";
            this.CinemaTabControl.SelectedIndex = 0;
            this.CinemaTabControl.Size = new System.Drawing.Size(957, 346);
            this.CinemaTabControl.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tabPage1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(7)))), ((int)(((byte)(44)))));
            this.tabPage1.Location = new System.Drawing.Point(4, 44);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage1.Size = new System.Drawing.Size(949, 298);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(981, 710);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.MenuStripMainForm);
            this.Controls.Add(this.CinemaTabControl);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ForeColor = System.Drawing.Color.CadetBlue;
            this.MainMenuStrip = this.MenuStripMainForm;
            this.MaximumSize = new System.Drawing.Size(999, 850);
            this.MinimumSize = new System.Drawing.Size(999, 700);
            this.Name = "MainForm";
            this.Text = "Сеть кинотеатров";
            this.MenuStripMainForm.ResumeLayout(false);
            this.MenuStripMainForm.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.CinemaTabControl.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MenuStripMainForm;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SaveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CloseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem управлениеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem кинотеатрToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AddCinemaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DeleteCinemaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem добавитьЗалToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblCinemaName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblStopTimeWork;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblStartTimeWork;
        private System.Windows.Forms.Label lblWebsite;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblAdress;
        private System.Windows.Forms.TabControl CinemaTabControl;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabPage tabPage1;
    }
}


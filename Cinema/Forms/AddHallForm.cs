﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cinema
{
    public partial class AddHallForm : Form
    {
        private string CinemaName;
        public AddHallForm(string name)
        {
            InitializeComponent();
            CinemaName = name;
        }

        //Нажатие на кнопку Добавить
        private void AddNewHall_Click(object sender, EventArgs e)
        {
            //Если поля заполнены
            if (txtBoxNewNameHall.TextLength>0 &&
                txtBoxNewRow.TextLength>0 &&
                txtBoxNewPlace.TextLength>0 )
            {
                if (Int32.Parse(txtBoxNewRow.Text) > 4 && Int32.Parse(txtBoxNewRow.Text) < 10 &&
                Int32.Parse(txtBoxNewPlace.Text) > 8 && Int32.Parse(txtBoxNewPlace.Text) < 14
                )
                {
                    Halls _hal = new Halls();
                    //Заполняем данные
                    _hal.hall_number = txtBoxNewNameHall.Text;
                    _hal.count_row = Int32.Parse(txtBoxNewRow.Text);
                    _hal.count_places = Int32.Parse(txtBoxNewPlace.Text);
                    _hal.count_seats = Int32.Parse(txtBoxNewPlace.Text) * Int32.Parse(txtBoxNewRow.Text);
                    //Добавляем данные
                    ContrDB.AddHallInCinema(CinemaName, _hal);
                    //Закрываем форму
                    Close();
                }
                else
                {
                    MessageBox.Show("Поля заполнены неверно!", "Ошибка");
                    txtBoxNewNameHall.Text = "";
                    txtBoxNewRow.Text = "";
                    txtBoxNewPlace.Text = "";
                }
            }
            //Если есть пустой поле
            else
            {
                MessageBox.Show("Не все поля заполнены!", "Поля не заполнены", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            txtBoxNewNameHall.Text = "";
            txtBoxNewRow.Text = "";
            txtBoxNewPlace.Text = "";
        }
    }
}

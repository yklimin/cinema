﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace Cinema
{
    public partial class MainForm : Form
    {
        MainContr control;
        Size panelSize;
        private string NameCinemaTheatre;
        private int CountHall;
        private int CountCinemaTheatre;

        public MainForm()
        {
            InitializeComponent();
            //Инициализируем доступный размер панели
            label7.Text = DateTime.Now.ToShortDateString();
            panelSize = new Size();
            //Инициализация контролера
            control = new MainContr();
            panelSize = tabPage1.Size;
            //Добавляем обработчик панели
            control.update += update;
            //Очищаем все вкладки
            CinemaTabControl.TabPages.Clear();
            //Добавляем обработчик событий переключения вкладки
            CinemaTabControl.SelectedIndexChanged += NewTab;
            //Заполняем информацию о кинотеатре
            CinemaTheatre cinema = new CinemaTheatre();
            cinema = control.GetCinema()[0];
            NameCinemaTheatre = cinema.cinema_name;
            CountHall = cinema.CountHall;
            lblCinemaName.Text = cinema.cinema_name;
            lblAdress.Text = cinema.address;
            lblPhone.Text = cinema.phone;
            lblWebsite.Text = cinema.website;
            lblStartTimeWork.Text = cinema.start_time_work;
            lblStopTimeWork.Text = cinema.stop_time_work;
            CountCinemaTheatre = control.CountCinema;
            //Обновляем
            update();
        }

        //Обновление главной формы
        private void update()
        {
            CinemaTabControl.TabPages.Clear();
            foreach(CinemaTheatre cinemaOffice in control.GetCinema())
            {
                TabPage tab = new TabPage();
                tab.Text = cinemaOffice.cinema_name;
                FlowLayoutPanel panel = new FlowLayoutPanel();
                panel.Size = panelSize;
                foreach(Halls hall in cinemaOffice.GetHall())
                {
                    Button newHall = new Button();
                    
                    Size size = new Size();
                    size.Width = 143;
                    size.Height = 100;
                    newHall.Size = size;
                    newHall.Text = hall.hall_number;
                    newHall.Click += ClickToHall;
                    panel.Controls.Add(newHall);
                }
                tab.Controls.Add(panel);
                CinemaTabControl.TabPages.Add(tab);
            }
        }

        //Обработчик переключения вкладки
        private void NewTab(object sender, EventArgs e)
        {
            CinemaTheatre cinema = new CinemaTheatre();
            if (CinemaTabControl.SelectedIndex < 0)
                cinema = control.GetCinema()[0];
            else
                cinema = control.GetCinema()[CinemaTabControl.SelectedIndex];
            //Переопределяем поля для корректной записи данных
            //Берём название фирмы, которое открыто в данный момент
            NameCinemaTheatre = cinema.cinema_name;
            //Берём количество залов, которое есть в кинотеатре
            CountHall = cinema.CountHall;
            lblCinemaName.Text = cinema.cinema_name;
            lblAdress.Text = cinema.address;
            lblPhone.Text = cinema.phone;
            lblWebsite.Text = cinema.website;
            lblStartTimeWork.Text = cinema.start_time_work;
            lblStopTimeWork.Text = cinema.stop_time_work;
        }

        //Метод обработки клика Зала
        private void ClickToHall(object obj,EventArgs e)
        {
            Button btm = (Button)obj;
            control.HallInfo(CinemaTabControl.SelectedTab.Text, btm.Text);
        }


        //Меню - сохранить БД
        private void SaveToolStripMenuItem_Click(object sender,EventArgs e)
        {
            control.Save();
        }

        //Меню - выход
        private void CloseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        //Меню - добавить кинотеатр
        private void AddCinemaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (CountCinemaTheatre < 11)
            {
                DialogResult result = MessageBox.Show("Вы точно хотите добавить кинотеатр?", "Подтверждение операции", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    control.AddCinemaTheatre();
                }
            }
            else
            {
                MessageBox.Show("Количество кинотеатров больше 10, добавление не возможно!", "Превышено число добавлений", MessageBoxButtons.OK);
            }
        }

        //Меню - удалить кинотеатр
        private void DeleteCinemaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Вы точно хотите удалить кинотеатр?", "Подтверждение операции", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(result == DialogResult.Yes)
            {
                control.DeleteCinemaTheatre();
            }
        }

        //Меню - добавить зал в кинотеатре
        private void NewHallToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (CountHall < 8)
            {

                DialogResult result = MessageBox.Show("Вы точно хотите добавить новый зал?", "Подтверждение операции", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    control.AddHall(NameCinemaTheatre);
                }
            }
            else
            //Если количество залов больше 8, то добавить мы больше не можем
            {
                MessageBox.Show("Извините, но вы больше не можите добавить зал!", "Добавление Зала", MessageBoxButtons.OK);
            }
        }

        private void lblCinemaName_Click(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void добавитьToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}

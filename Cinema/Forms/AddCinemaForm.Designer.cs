﻿namespace Cinema
{
    partial class AddCinemaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ClearButton = new System.Windows.Forms.Button();
            this.txtBoxNewStopTimeWork = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBoxNewStartTimeWork = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBoxNewWebSite = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBoxNewNumber = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBoxNewNameCinema = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.AddNewCinemaTheatre = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBoxNewAddress = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // ClearButton
            // 
            this.ClearButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ClearButton.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ClearButton.Location = new System.Drawing.Point(328, 302);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(142, 38);
            this.ClearButton.TabIndex = 23;
            this.ClearButton.Text = "Очистить";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // txtBoxNewStopTimeWork
            // 
            this.txtBoxNewStopTimeWork.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtBoxNewStopTimeWork.Location = new System.Drawing.Point(200, 221);
            this.txtBoxNewStopTimeWork.Name = "txtBoxNewStopTimeWork";
            this.txtBoxNewStopTimeWork.Size = new System.Drawing.Size(270, 37);
            this.txtBoxNewStopTimeWork.TabIndex = 22;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(12, 224);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(121, 35);
            this.label5.TabIndex = 21;
            this.label5.Text = "Конец работы";
            // 
            // txtBoxNewStartTimeWork
            // 
            this.txtBoxNewStartTimeWork.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtBoxNewStartTimeWork.Location = new System.Drawing.Point(200, 178);
            this.txtBoxNewStartTimeWork.Name = "txtBoxNewStartTimeWork";
            this.txtBoxNewStartTimeWork.Size = new System.Drawing.Size(270, 37);
            this.txtBoxNewStartTimeWork.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 181);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(132, 35);
            this.label4.TabIndex = 19;
            this.label4.Text = "Начало работы";
            // 
            // txtBoxNewWebSite
            // 
            this.txtBoxNewWebSite.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtBoxNewWebSite.Location = new System.Drawing.Point(200, 92);
            this.txtBoxNewWebSite.Name = "txtBoxNewWebSite";
            this.txtBoxNewWebSite.Size = new System.Drawing.Size(270, 37);
            this.txtBoxNewWebSite.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(12, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(139, 35);
            this.label3.TabIndex = 17;
            this.label3.Text = "Сайт кинотеатра";
            // 
            // txtBoxNewNumber
            // 
            this.txtBoxNewNumber.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtBoxNewNumber.Location = new System.Drawing.Point(200, 49);
            this.txtBoxNewNumber.Name = "txtBoxNewNumber";
            this.txtBoxNewNumber.Size = new System.Drawing.Size(270, 37);
            this.txtBoxNewNumber.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 35);
            this.label2.TabIndex = 15;
            this.label2.Text = "Контактный номер";
            // 
            // txtBoxNewNameCinema
            // 
            this.txtBoxNewNameCinema.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtBoxNewNameCinema.Location = new System.Drawing.Point(200, 6);
            this.txtBoxNewNameCinema.Name = "txtBoxNewNameCinema";
            this.txtBoxNewNameCinema.Size = new System.Drawing.Size(270, 37);
            this.txtBoxNewNameCinema.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 35);
            this.label1.TabIndex = 13;
            this.label1.Text = "Название кинотеатра";
            // 
            // AddNewCinemaTheatre
            // 
            this.AddNewCinemaTheatre.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.AddNewCinemaTheatre.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddNewCinemaTheatre.Location = new System.Drawing.Point(18, 302);
            this.AddNewCinemaTheatre.Name = "AddNewCinemaTheatre";
            this.AddNewCinemaTheatre.Size = new System.Drawing.Size(142, 38);
            this.AddNewCinemaTheatre.TabIndex = 12;
            this.AddNewCinemaTheatre.Text = "Добавить";
            this.AddNewCinemaTheatre.UseVisualStyleBackColor = true;
            this.AddNewCinemaTheatre.Click += new System.EventHandler(this.AddNewCinemaTheatre_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(12, 138);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 35);
            this.label6.TabIndex = 24;
            this.label6.Text = "Адресс";
            // 
            // txtBoxNewAddress
            // 
            this.txtBoxNewAddress.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtBoxNewAddress.Location = new System.Drawing.Point(200, 135);
            this.txtBoxNewAddress.Name = "txtBoxNewAddress";
            this.txtBoxNewAddress.Size = new System.Drawing.Size(270, 37);
            this.txtBoxNewAddress.TabIndex = 25;
            // 
            // AddCinemaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(492, 363);
            this.ControlBox = false;
            this.Controls.Add(this.txtBoxNewAddress);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.txtBoxNewStopTimeWork);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtBoxNewStartTimeWork);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtBoxNewWebSite);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBoxNewNumber);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtBoxNewNameCinema);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AddNewCinemaTheatre);
            this.MaximumSize = new System.Drawing.Size(510, 410);
            this.MinimumSize = new System.Drawing.Size(510, 410);
            this.Name = "AddCinemaForm";
            this.Text = "Добавление кинотеатра";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.TextBox txtBoxNewStopTimeWork;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBoxNewStartTimeWork;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBoxNewWebSite;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBoxNewNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBoxNewNameCinema;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button AddNewCinemaTheatre;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtBoxNewAddress;
    }
}
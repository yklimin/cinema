﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Cinema
{
    public partial class InfoAboutHall : Form
    {
        //Выбранные зал
        Halls halls;
        //Нужно для дальнейшей реализации
        CinemaTheatre cinema_name;
        //Контролер
        ContrHall control;
        //Для обработки случаев с временем работы
        string time_open;
        string time_close;
        List<int> count = new List<int>();

        //Конструктор
        public InfoAboutHall(CinemaTheatre _cinema_name, Halls _hall,string _time_open,string _time_close)
        {
            InitializeComponent();
            //Инициализация зала
            halls = new Halls();
            halls = _hall;
            //Обработка времени
            time_open = _time_open;
            time_close = _time_close;

            cinema_name = _cinema_name;
            //Обработчик события обновления
            control = new ContrHall(halls, cinema_name.cinema_name);
            control.update += UpdateTable;
             
            lblHallName.Text = halls.hall_number;
            lblCountSeats.Text = halls.count_seats.ToString()+" мест";
            lblPlace.Text = "0";
            lblPrice.Text = "0 руб.";

            int index = 0;
            //Добавление в таблицу данныхф
            foreach (Films film in halls.GetFilm())
            {
                count.Add(film.CountSeats);
                string _date_session = film.date_session.ToShortDateString().ToString();
                string _film_name = film.film_name;
                string _start_time_film = film.start_time_film;
                string _duration = film.duration;
                string _price = film.price.ToString();
                dataGridView1.Rows.Add(_date_session, _film_name, _start_time_film, _duration, _price);
                dataGridView1.Rows[index++].ReadOnly = true;
            }

            MessageBox.Show("При добавлении фильма учтите некоторые тонкости. " +
                "Фильм можно будет добавить, если после его окончания прошло не менее 15 минут. " +
                "Это сделано для того, чтобы персонал успел убрать зал после фильма. " +
                "При удалении фильма учтите, что нельзя удалять фильм, который в данный момент показывается! " +
                "Также не забудьте учитывать время работы кинотеатра. ", "Информация о удалении и добавлении", MessageBoxButtons.OK);
        }

        //Добавление фильма
        private void NewFilm_Click(object sender, EventArgs e)
        {
            if (txtBoxFilmName.TextLength > 0 &&
                txtBoxStartTimeFilm.TextLength > 0 &&
                txtBoxDuration.TextLength > 0 &&
                txtBoxPrice.TextLength > 0)
            {
                string _date_session = dateTimePicker.Value.ToShortDateString().ToString();
                string _film_name = txtBoxFilmName.Text;
                string _start_time_film = txtBoxStartTimeFilm.Text;
                string _duration = txtBoxDuration.Text;
                string _price = txtBoxPrice.Text;

                bool res = control.AddFilmToHall(_date_session, _film_name, _start_time_film, _duration, _price, time_open, time_close);

                if(!res)
                {
                    MessageBox.Show("Вы ввели неверные данные, проверьте их. " +
                        "Возможные проблемы: " +
                        "1. Вы хотите добавить новый фильм к уже прошедшей дате; " +
                        "2. Добавляемый фильм заходит на время сеанса другого фильма.", 
                        "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                UpdateTable();
                
                dateTimePicker.Value = DateTime.Parse(DateTime.Now.ToShortDateString());
                txtBoxFilmName.Text = "";
                txtBoxStartTimeFilm.Text = "";
                txtBoxDuration.Text = "";
                txtBoxPrice.Text = "";
            }
            else
            {
                MessageBox.Show("Не все поля заполнены!", "Поля не заполнены", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Обновление таблицы после добавления/удаления
        private void UpdateTable()
        {
            List<int> new_count= new List<int>();
            dataGridView1.Rows.Clear();
            count = new_count;
            int index = 0;
            foreach(Films film in halls.GetFilm())
            {
                string _date_session = film.date_session.ToShortDateString().ToString();
                string _film_name = film.film_name;
                string _start_time_film = film.start_time_film;
                string _duration = film.duration;
                string _price = film.price.ToString();
                count.Add(film.CountSeats);
                dataGridView1.Rows.Add(_date_session, _film_name, _start_time_film, _duration, _price);
                dataGridView1.Rows[index++].ReadOnly = true;
            }
            dateTimePicker.Value = DateTime.Parse(DateTime.Now.ToShortDateString());
            txtBoxFilmName.Text = "";
            txtBoxStartTimeFilm.Text = "";
            txtBoxDuration.Text = "";
            txtBoxPrice.Text = "";

            lblCountSeats.Text = halls.count_seats.ToString() + " мест";
            lblPlace.Text = "0 мест";
            lblPrice.Text = "0 рублей";
        }

        //Наведение курсора на таблицу
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int n = e.RowIndex;

            if(n!=-1)
            {
                string DateToParse = dataGridView1.Rows[n].Cells[0].Value.ToString();
                dateTimePicker.Value = DateTime.Parse(control.ParseTime(DateToParse, "00:00").ToShortDateString());
                txtBoxFilmName.Text = dataGridView1.Rows[n].Cells[1].Value.ToString();
                txtBoxStartTimeFilm.Text = dataGridView1.Rows[n].Cells[2].Value.ToString();
                txtBoxDuration.Text = dataGridView1.Rows[n].Cells[3].Value.ToString();
                txtBoxPrice.Text = dataGridView1.Rows[n].Cells[4].Value.ToString();

                lblPlace.Text = (halls.count_seats-count[n]).ToString();
                lblPrice.Text = (count[n]* Int32.Parse(dataGridView1.Rows[n].Cells[4].Value.ToString())).ToString()+" руб.";
            }

        }

        //Удаление фильма
        private void DeleteFilm_Click(object sender, EventArgs e)
        {
            if (txtBoxFilmName.TextLength > 0 &&
                txtBoxStartTimeFilm.TextLength > 0 &&
                txtBoxDuration.TextLength > 0 &&
                txtBoxPrice.TextLength > 0)
            {
                string _date_session = dateTimePicker.Value.ToShortDateString().ToString();
                string _film_name = txtBoxFilmName.Text;
                string _start_time_film = txtBoxStartTimeFilm.Text;
                string _duration = txtBoxDuration.Text;
                string _price = txtBoxPrice.Text;

                bool res = control.DeleteFilmInHall(_date_session, _film_name, _start_time_film, _duration, _price);
               
                if (!res)
                {
                    MessageBox.Show("Нельзя удалить фильм который сейчас идет в зале!",
                        "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                UpdateTable();

                dateTimePicker.Value = DateTime.Parse(DateTime.Now.ToShortDateString());
                txtBoxFilmName.Text = "";
                txtBoxStartTimeFilm.Text = "";
                txtBoxDuration.Text = "";
                txtBoxPrice.Text = "";
            }


        }

        //Удаление зала
        private void DeleteHall_Click(object sender, EventArgs e)
        {
            control.DelHall();
            Close();
        }


        //Кнопка очистки
        private void ClearBut_Click(object sender, EventArgs e)
        {
            dateTimePicker.Value = DateTime.Parse(DateTime.Now.ToShortDateString());
            txtBoxFilmName.Text = "";
            txtBoxStartTimeFilm.Text = "";
            txtBoxDuration.Text = "";
            txtBoxPrice.Text = "";

            lblCountSeats.Text = halls.count_seats.ToString() + " мест";
            lblPlace.Text = "0 мест";
            lblPrice.Text = "0 рублей";
        }

        //Выбрать места
        private void buttonToSessions_Click(object sender, EventArgs e)
        {
            if (txtBoxFilmName.TextLength > 0 &&
                txtBoxStartTimeFilm.TextLength > 0 &&
                txtBoxDuration.TextLength > 0 &&
                txtBoxPrice.TextLength > 0)
            {
                Films film = new Films();
                DateTime _date_session = control.ParseTime(dateTimePicker.Value.ToShortDateString(),"00:00");
                string _film_name = txtBoxFilmName.Text;
                string _start_time_film = txtBoxStartTimeFilm.Text;
                string _duration = txtBoxDuration.Text;
                int _price = Int32.Parse(txtBoxPrice.Text);

                foreach(Films flm in halls.GetFilm())
                {
                    if (flm.film_name == _film_name &&
                                    flm.date_session == _date_session &&
                                    flm.duration == _duration &&
                                    flm.start_time_film == _start_time_film &&
                                    flm.price == _price)
                    {
                        film = flm;
                    }
                }

                DateTime check = control.ParseTime(dateTimePicker.Value.ToShortDateString().ToString(), txtBoxStartTimeFilm.Text);
                bool res = true;
                if (check < DateTime.Now)
                    res = false;

                SessionsForm film_to_session = new SessionsForm(cinema_name.cinema_name, halls.hall_number,film,halls.count_row,halls.count_places,res);
                film_to_session.Show();
            }
            else
            {
                MessageBox.Show("Вы не выбрали фильм к которому хотите перейти!", "Ошибка", MessageBoxButtons.OK);
            }

            UpdateTable();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}

﻿namespace Cinema
{
    partial class DeleteCinemaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DeleteCinemaTheatre = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBoxDeleteNameCinema = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // DeleteCinemaTheatre
            // 
            this.DeleteCinemaTheatre.AllowDrop = true;
            this.DeleteCinemaTheatre.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.DeleteCinemaTheatre.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DeleteCinemaTheatre.Location = new System.Drawing.Point(17, 103);
            this.DeleteCinemaTheatre.Name = "DeleteCinemaTheatre";
            this.DeleteCinemaTheatre.Size = new System.Drawing.Size(142, 38);
            this.DeleteCinemaTheatre.TabIndex = 10;
            this.DeleteCinemaTheatre.Text = "Удалить";
            this.DeleteCinemaTheatre.UseVisualStyleBackColor = true;
            this.DeleteCinemaTheatre.Click += new System.EventHandler(this.DeleteCinemaTheatre_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(526, 35);
            this.label2.TabIndex = 9;
            this.label2.Text = "(Введите название кинотеатра, которое вы хотите удалить из базы)";
            // 
            // txtBoxDeleteNameCinema
            // 
            this.txtBoxDeleteNameCinema.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtBoxDeleteNameCinema.Location = new System.Drawing.Point(199, 6);
            this.txtBoxDeleteNameCinema.Name = "txtBoxDeleteNameCinema";
            this.txtBoxDeleteNameCinema.Size = new System.Drawing.Size(339, 37);
            this.txtBoxDeleteNameCinema.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(11, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 35);
            this.label1.TabIndex = 7;
            this.label1.Text = "Название кинотеатра";
            // 
            // DeleteCinemaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(582, 153);
            this.ControlBox = false;
            this.Controls.Add(this.DeleteCinemaTheatre);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtBoxDeleteNameCinema);
            this.Controls.Add(this.label1);
            this.MaximumSize = new System.Drawing.Size(600, 200);
            this.MinimumSize = new System.Drawing.Size(600, 200);
            this.Name = "DeleteCinemaForm";
            this.Text = "Удалить Кинотеатр";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button DeleteCinemaTheatre;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBoxDeleteNameCinema;
        private System.Windows.Forms.Label label1;
    }
}
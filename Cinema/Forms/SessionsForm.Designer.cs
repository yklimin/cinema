﻿namespace Cinema
{
    partial class SessionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddNewSession = new System.Windows.Forms.Button();
            this.lblLevel1Left = new System.Windows.Forms.Label();
            this.lblLevel2Left = new System.Windows.Forms.Label();
            this.lblLevel3Left = new System.Windows.Forms.Label();
            this.lblLevel1Right = new System.Windows.Forms.Label();
            this.lblLevel4Left = new System.Windows.Forms.Label();
            this.lblLevel5Left = new System.Windows.Forms.Label();
            this.lblLevel6Left = new System.Windows.Forms.Label();
            this.lblLevel7Left = new System.Windows.Forms.Label();
            this.lblLevel8Left = new System.Windows.Forms.Label();
            this.lblLevel9Left = new System.Windows.Forms.Label();
            this.lblLevel2Right = new System.Windows.Forms.Label();
            this.lblLevel3Right = new System.Windows.Forms.Label();
            this.lblLevel4Right = new System.Windows.Forms.Label();
            this.lblLevel5Right = new System.Windows.Forms.Label();
            this.lblLevel6Right = new System.Windows.Forms.Label();
            this.lblLevel7Right = new System.Windows.Forms.Label();
            this.lblLevel8Right = new System.Windows.Forms.Label();
            this.lblLevel9Right = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblStartFilm = new System.Windows.Forms.Label();
            this.lblFilmName = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // AddNewSession
            // 
            this.AddNewSession.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.AddNewSession.BackColor = System.Drawing.Color.White;
            this.AddNewSession.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddNewSession.ForeColor = System.Drawing.SystemColors.WindowText;
            this.AddNewSession.Location = new System.Drawing.Point(435, 722);
            this.AddNewSession.Name = "AddNewSession";
            this.AddNewSession.Size = new System.Drawing.Size(142, 38);
            this.AddNewSession.TabIndex = 0;
            this.AddNewSession.Text = "Выбрать";
            this.AddNewSession.UseVisualStyleBackColor = false;
            this.AddNewSession.Click += new System.EventHandler(this.AddNewSession_Click);
            // 
            // lblLevel1Left
            // 
            this.lblLevel1Left.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLevel1Left.AutoSize = true;
            this.lblLevel1Left.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLevel1Left.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblLevel1Left.Location = new System.Drawing.Point(12, 70);
            this.lblLevel1Left.Name = "lblLevel1Left";
            this.lblLevel1Left.Size = new System.Drawing.Size(47, 29);
            this.lblLevel1Left.TabIndex = 1;
            this.lblLevel1Left.Text = "1 ряд";
            this.lblLevel1Left.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLevel2Left
            // 
            this.lblLevel2Left.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLevel2Left.AutoSize = true;
            this.lblLevel2Left.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLevel2Left.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblLevel2Left.Location = new System.Drawing.Point(12, 130);
            this.lblLevel2Left.Name = "lblLevel2Left";
            this.lblLevel2Left.Size = new System.Drawing.Size(48, 29);
            this.lblLevel2Left.TabIndex = 2;
            this.lblLevel2Left.Text = "2 ряд";
            // 
            // lblLevel3Left
            // 
            this.lblLevel3Left.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLevel3Left.AutoSize = true;
            this.lblLevel3Left.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLevel3Left.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblLevel3Left.Location = new System.Drawing.Point(12, 190);
            this.lblLevel3Left.Name = "lblLevel3Left";
            this.lblLevel3Left.Size = new System.Drawing.Size(48, 29);
            this.lblLevel3Left.TabIndex = 3;
            this.lblLevel3Left.Text = "3 ряд";
            // 
            // lblLevel1Right
            // 
            this.lblLevel1Right.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLevel1Right.AutoSize = true;
            this.lblLevel1Right.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLevel1Right.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblLevel1Right.Location = new System.Drawing.Point(929, 70);
            this.lblLevel1Right.Name = "lblLevel1Right";
            this.lblLevel1Right.Size = new System.Drawing.Size(47, 29);
            this.lblLevel1Right.TabIndex = 4;
            this.lblLevel1Right.Text = "1 ряд";
            this.lblLevel1Right.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLevel4Left
            // 
            this.lblLevel4Left.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLevel4Left.AutoSize = true;
            this.lblLevel4Left.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLevel4Left.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblLevel4Left.Location = new System.Drawing.Point(12, 250);
            this.lblLevel4Left.Name = "lblLevel4Left";
            this.lblLevel4Left.Size = new System.Drawing.Size(48, 29);
            this.lblLevel4Left.TabIndex = 5;
            this.lblLevel4Left.Text = "4 ряд";
            // 
            // lblLevel5Left
            // 
            this.lblLevel5Left.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLevel5Left.AutoSize = true;
            this.lblLevel5Left.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLevel5Left.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblLevel5Left.Location = new System.Drawing.Point(12, 315);
            this.lblLevel5Left.Name = "lblLevel5Left";
            this.lblLevel5Left.Size = new System.Drawing.Size(48, 29);
            this.lblLevel5Left.TabIndex = 6;
            this.lblLevel5Left.Text = "5 ряд";
            // 
            // lblLevel6Left
            // 
            this.lblLevel6Left.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLevel6Left.AutoSize = true;
            this.lblLevel6Left.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLevel6Left.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblLevel6Left.Location = new System.Drawing.Point(12, 375);
            this.lblLevel6Left.Name = "lblLevel6Left";
            this.lblLevel6Left.Size = new System.Drawing.Size(49, 29);
            this.lblLevel6Left.TabIndex = 7;
            this.lblLevel6Left.Text = "6 ряд";
            // 
            // lblLevel7Left
            // 
            this.lblLevel7Left.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLevel7Left.AutoSize = true;
            this.lblLevel7Left.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLevel7Left.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblLevel7Left.Location = new System.Drawing.Point(12, 435);
            this.lblLevel7Left.Name = "lblLevel7Left";
            this.lblLevel7Left.Size = new System.Drawing.Size(47, 29);
            this.lblLevel7Left.TabIndex = 8;
            this.lblLevel7Left.Text = "7 ряд";
            // 
            // lblLevel8Left
            // 
            this.lblLevel8Left.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLevel8Left.AutoSize = true;
            this.lblLevel8Left.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLevel8Left.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblLevel8Left.Location = new System.Drawing.Point(12, 500);
            this.lblLevel8Left.Name = "lblLevel8Left";
            this.lblLevel8Left.Size = new System.Drawing.Size(48, 29);
            this.lblLevel8Left.TabIndex = 9;
            this.lblLevel8Left.Text = "8 ряд";
            // 
            // lblLevel9Left
            // 
            this.lblLevel9Left.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLevel9Left.AutoSize = true;
            this.lblLevel9Left.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLevel9Left.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblLevel9Left.Location = new System.Drawing.Point(12, 560);
            this.lblLevel9Left.Name = "lblLevel9Left";
            this.lblLevel9Left.Size = new System.Drawing.Size(49, 29);
            this.lblLevel9Left.TabIndex = 10;
            this.lblLevel9Left.Text = "9 ряд";
            // 
            // lblLevel2Right
            // 
            this.lblLevel2Right.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLevel2Right.AutoSize = true;
            this.lblLevel2Right.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLevel2Right.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblLevel2Right.Location = new System.Drawing.Point(929, 130);
            this.lblLevel2Right.Name = "lblLevel2Right";
            this.lblLevel2Right.Size = new System.Drawing.Size(48, 29);
            this.lblLevel2Right.TabIndex = 11;
            this.lblLevel2Right.Text = "2 ряд";
            // 
            // lblLevel3Right
            // 
            this.lblLevel3Right.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLevel3Right.AutoSize = true;
            this.lblLevel3Right.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLevel3Right.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblLevel3Right.Location = new System.Drawing.Point(929, 190);
            this.lblLevel3Right.Name = "lblLevel3Right";
            this.lblLevel3Right.Size = new System.Drawing.Size(48, 29);
            this.lblLevel3Right.TabIndex = 12;
            this.lblLevel3Right.Text = "3 ряд";
            // 
            // lblLevel4Right
            // 
            this.lblLevel4Right.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLevel4Right.AutoSize = true;
            this.lblLevel4Right.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLevel4Right.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblLevel4Right.Location = new System.Drawing.Point(929, 250);
            this.lblLevel4Right.Name = "lblLevel4Right";
            this.lblLevel4Right.Size = new System.Drawing.Size(48, 29);
            this.lblLevel4Right.TabIndex = 13;
            this.lblLevel4Right.Text = "4 ряд";
            // 
            // lblLevel5Right
            // 
            this.lblLevel5Right.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLevel5Right.AutoSize = true;
            this.lblLevel5Right.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLevel5Right.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblLevel5Right.Location = new System.Drawing.Point(929, 315);
            this.lblLevel5Right.Name = "lblLevel5Right";
            this.lblLevel5Right.Size = new System.Drawing.Size(48, 29);
            this.lblLevel5Right.TabIndex = 14;
            this.lblLevel5Right.Text = "5 ряд";
            // 
            // lblLevel6Right
            // 
            this.lblLevel6Right.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLevel6Right.AutoSize = true;
            this.lblLevel6Right.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLevel6Right.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblLevel6Right.Location = new System.Drawing.Point(929, 375);
            this.lblLevel6Right.Name = "lblLevel6Right";
            this.lblLevel6Right.Size = new System.Drawing.Size(49, 29);
            this.lblLevel6Right.TabIndex = 15;
            this.lblLevel6Right.Text = "6 ряд";
            // 
            // lblLevel7Right
            // 
            this.lblLevel7Right.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLevel7Right.AutoSize = true;
            this.lblLevel7Right.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLevel7Right.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblLevel7Right.Location = new System.Drawing.Point(929, 435);
            this.lblLevel7Right.Name = "lblLevel7Right";
            this.lblLevel7Right.Size = new System.Drawing.Size(47, 29);
            this.lblLevel7Right.TabIndex = 16;
            this.lblLevel7Right.Text = "7 ряд";
            // 
            // lblLevel8Right
            // 
            this.lblLevel8Right.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLevel8Right.AutoSize = true;
            this.lblLevel8Right.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLevel8Right.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblLevel8Right.Location = new System.Drawing.Point(929, 500);
            this.lblLevel8Right.Name = "lblLevel8Right";
            this.lblLevel8Right.Size = new System.Drawing.Size(48, 29);
            this.lblLevel8Right.TabIndex = 17;
            this.lblLevel8Right.Text = "8 ряд";
            // 
            // lblLevel9Right
            // 
            this.lblLevel9Right.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLevel9Right.AutoSize = true;
            this.lblLevel9Right.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLevel9Right.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblLevel9Right.Location = new System.Drawing.Point(929, 560);
            this.lblLevel9Right.Name = "lblLevel9Right";
            this.lblLevel9Right.Size = new System.Drawing.Size(49, 29);
            this.lblLevel9Right.TabIndex = 18;
            this.lblLevel9Right.Text = "9 ряд";
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkBox1.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBox1.AutoSize = true;
            this.checkBox1.BackColor = System.Drawing.Color.Green;
            this.checkBox1.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBox1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.checkBox1.Location = new System.Drawing.Point(802, 630);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(31, 39);
            this.checkBox1.TabIndex = 19;
            this.checkBox1.Text = "  ";
            this.checkBox1.UseVisualStyleBackColor = false;
            // 
            // checkBox2
            // 
            this.checkBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkBox2.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBox2.AutoSize = true;
            this.checkBox2.BackColor = System.Drawing.Color.Yellow;
            this.checkBox2.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBox2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.checkBox2.Location = new System.Drawing.Point(802, 679);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(31, 39);
            this.checkBox2.TabIndex = 20;
            this.checkBox2.Text = "  ";
            this.checkBox2.UseVisualStyleBackColor = false;
            // 
            // checkBox3
            // 
            this.checkBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkBox3.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBox3.AutoSize = true;
            this.checkBox3.BackColor = System.Drawing.Color.Red;
            this.checkBox3.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBox3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.checkBox3.Location = new System.Drawing.Point(802, 729);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(31, 39);
            this.checkBox3.TabIndex = 21;
            this.checkBox3.Text = "  ";
            this.checkBox3.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label1.Location = new System.Drawing.Point(834, 640);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 29);
            this.label1.TabIndex = 22;
            this.label1.Text = "Свободные места ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label2.Location = new System.Drawing.Point(834, 689);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 29);
            this.label2.TabIndex = 23;
            this.label2.Text = "Выбранные места";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Oswald", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label3.Location = new System.Drawing.Point(836, 739);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(127, 29);
            this.label3.TabIndex = 24;
            this.label3.Text = "Проданные места";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(996, 38);
            this.pictureBox1.TabIndex = 25;
            this.pictureBox1.TabStop = false;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.Control;
            this.label4.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label4.Location = new System.Drawing.Point(466, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 35);
            this.label4.TabIndex = 26;
            this.label4.Text = "Экран";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDate
            // 
            this.lblDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Oswald", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblDate.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblDate.Location = new System.Drawing.Point(168, 679);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(94, 32);
            this.lblDate.TabIndex = 27;
            this.lblDate.Text = "18.12.2019";
            // 
            // lblStartFilm
            // 
            this.lblStartFilm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblStartFilm.AutoSize = true;
            this.lblStartFilm.Font = new System.Drawing.Font("Oswald", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblStartFilm.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblStartFilm.Location = new System.Drawing.Point(168, 728);
            this.lblStartFilm.Name = "lblStartFilm";
            this.lblStartFilm.Size = new System.Drawing.Size(55, 32);
            this.lblStartFilm.TabIndex = 28;
            this.lblStartFilm.Text = "07:30";
            // 
            // lblFilmName
            // 
            this.lblFilmName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblFilmName.AutoSize = true;
            this.lblFilmName.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblFilmName.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblFilmName.Location = new System.Drawing.Point(168, 630);
            this.lblFilmName.Name = "lblFilmName";
            this.lblFilmName.Size = new System.Drawing.Size(67, 35);
            this.lblFilmName.TabIndex = 29;
            this.lblFilmName.Text = "Аватар";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label5.Location = new System.Drawing.Point(12, 630);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 35);
            this.label5.TabIndex = 30;
            this.label5.Text = "Фильм:";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label6.Location = new System.Drawing.Point(12, 676);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 35);
            this.label6.TabIndex = 31;
            this.label6.Text = "Дата сеанса:";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label7.Location = new System.Drawing.Point(12, 725);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 35);
            this.label7.TabIndex = 32;
            this.label7.Text = "Время сеанса:";
            // 
            // SessionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(996, 803);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblFilmName);
            this.Controls.Add(this.lblStartFilm);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.lblLevel9Right);
            this.Controls.Add(this.lblLevel8Right);
            this.Controls.Add(this.lblLevel7Right);
            this.Controls.Add(this.lblLevel6Right);
            this.Controls.Add(this.lblLevel5Right);
            this.Controls.Add(this.lblLevel4Right);
            this.Controls.Add(this.lblLevel3Right);
            this.Controls.Add(this.lblLevel2Right);
            this.Controls.Add(this.lblLevel9Left);
            this.Controls.Add(this.lblLevel8Left);
            this.Controls.Add(this.lblLevel7Left);
            this.Controls.Add(this.lblLevel6Left);
            this.Controls.Add(this.lblLevel5Left);
            this.Controls.Add(this.lblLevel4Left);
            this.Controls.Add(this.lblLevel1Right);
            this.Controls.Add(this.lblLevel3Left);
            this.Controls.Add(this.lblLevel2Left);
            this.Controls.Add(this.lblLevel1Left);
            this.Controls.Add(this.AddNewSession);
            this.ForeColor = System.Drawing.SystemColors.WindowText;
            this.MaximumSize = new System.Drawing.Size(1024, 850);
            this.MinimumSize = new System.Drawing.Size(750, 850);
            this.Name = "SessionsForm";
            this.Text = "Выбор мест";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AddNewSession;
        private System.Windows.Forms.Label lblLevel1Left;
        private System.Windows.Forms.Label lblLevel2Left;
        private System.Windows.Forms.Label lblLevel3Left;
        private System.Windows.Forms.Label lblLevel1Right;
        private System.Windows.Forms.Label lblLevel4Left;
        private System.Windows.Forms.Label lblLevel5Left;
        private System.Windows.Forms.Label lblLevel6Left;
        private System.Windows.Forms.Label lblLevel7Left;
        private System.Windows.Forms.Label lblLevel8Left;
        private System.Windows.Forms.Label lblLevel9Left;
        private System.Windows.Forms.Label lblLevel2Right;
        private System.Windows.Forms.Label lblLevel3Right;
        private System.Windows.Forms.Label lblLevel4Right;
        private System.Windows.Forms.Label lblLevel5Right;
        private System.Windows.Forms.Label lblLevel6Right;
        private System.Windows.Forms.Label lblLevel7Right;
        private System.Windows.Forms.Label lblLevel8Right;
        private System.Windows.Forms.Label lblLevel9Right;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblStartFilm;
        private System.Windows.Forms.Label lblFilmName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}